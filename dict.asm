%include "lib.inc"
%include "words.inc"

global find_word

section .text

find_word:
	; rdi - указатель на строку
	; rsi - указатель на начало словаря
	push r12
	push r13
	mov r12, rdi
	mov r13, rsi

	.next:
		mov rax, r13
		test rax, rax
		jz .bad_end

		lea rdi, [r13 + 8]
		mov rsi, r12
		call string_equals
		test rax, rax 
		
		jnz .good_end
		mov r13, [r13]
		jmp .next
		
	.good_end:
		lea rax, [r13 + 8]
		jmp .restore
	
	.bad_end:
		mov rax, 0
		jmp .restore

	.restore:
		pop r13
		pop r12
		ret
