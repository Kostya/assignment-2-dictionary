nasmFlags = -g -felf64 -o

.PHONY: clean test
all: main clean test
clean:
	rm *.o

%.o: %.asm
	nasm $(nasmFlags) $@ $<

dict.o: dict.asm lib.inc

main.o: main.asm lib.inc dict.inc words.inc

main: lib.o dict.o main.o
	ld -o $@ $?

test: main
	python test.py
