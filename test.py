# coding=utf-8
import subprocess

file_name = "./main"
input_data = ["first", "fourth", "", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"]
correct_stdout = ["your value is: 1", "", "", ""]
correct_stderr = ["", "word not found", "word not found", "buffer overflow"]
correct_message = "All tests passed"
wrong_message = "Not all tests passed"
isPassed = True

for i in range(4):
    process = subprocess.Popen(file_name, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    process.stdin.write(input_data[i])

    stdout, stderr = process.communicate()
    stdout = stdout.strip()
    stderr = stderr.strip()
    if correct_stdout[i] == stdout and correct_stderr[i] == stderr:
        print "Test ", i + 1, ": passed"
    else:
        print "Test ", i + 1, ": failed"
        print "Correct output: ", correct_stdout[i]
        print "Your output: ", stdout
        print "Correct error output: ", correct_stderr[i]
        print "Your error output: ", stderr
        isPassed = False

if isPassed:
    print correct_message
else:
    print wrong_message
