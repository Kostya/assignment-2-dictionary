%define size 256
%include "lib.inc"
%include "dict.inc"
%include "words.inc"

section .rodata

err_size_message: db "buffer overflow", 10, 0
err_not_found_message: db "word not found", 10, 0
value_found_message: db "your value is: ", 0

section .bss
string: resb size
 
section .text

global _start
	_start:		
		;чтение строки в буфер
		mov rdi, string
		mov rsi, size - 1
		call read_word
		test rax, rax
		jz .err_size

		;поиск слова
		mov rdi, rax
		mov rsi, end
		call find_word
		test rax, rax
		jz .err_not_found

		;вывод значения
		push rax
		mov rdi, value_found_message
		call print_string
		mov rdi, [rsp]
		call string_length
		add rax, [rsp]
		inc rax
		mov rdi, rax
		call print_string
		call print_newline
		pop rax
		jmp .end

		.err_size:
			mov rdi, err_size_message
			jmp .print_error
			
		.err_not_found:
			mov rdi, err_not_found_message
			jmp .print_error

		.print_error:
			call print_error_message
		.end:
			xor rdi, rdi
			jmp exit
